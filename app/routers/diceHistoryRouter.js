//Khai báo thư viện express:
const express = require('express');
const { createDiceHistory, getAllDiceHistories, getDiceHistoryById, updateDiceHistoryById, deleteDiceHistoryById } = require('../controllers/diceHistoryController');

//Khai báo  router:
const diceHistoryRouter = express.Router();


diceHistoryRouter.post("/dice-histories", createDiceHistory)

diceHistoryRouter.get("/dice-histories", getAllDiceHistories)

diceHistoryRouter.get("/dice-histories/:diceHistoryId", getDiceHistoryById)

diceHistoryRouter.put("/dice-histories/:diceHistoryId", updateDiceHistoryById)

diceHistoryRouter.delete("/dice-histories/:diceHistoryId", deleteDiceHistoryById)


module.exports = { diceHistoryRouter }