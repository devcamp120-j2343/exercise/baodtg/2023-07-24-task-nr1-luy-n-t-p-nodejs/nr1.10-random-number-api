//Khai báo thư viện express:
const express = require('express');
const { createVoucherHistory, getAllVoucherHistory, getVoucherHistoryById, updateVoucherHistoryById, deleteVoucherHistoryById } = require('../controllers/voucherHistoryController');

//Khai báo  router:
const voucherHistoryRouter = express.Router();


voucherHistoryRouter.post("/voucher-histories", createVoucherHistory)

voucherHistoryRouter.get("/voucher-histories", getAllVoucherHistory)

voucherHistoryRouter.get("/voucher-histories/:voucherHistoryId", getVoucherHistoryById)

voucherHistoryRouter.put("/voucher-histories/:voucherHistoryId", updateVoucherHistoryById)

voucherHistoryRouter.delete("/voucher-histories/:voucherHistoryId", deleteVoucherHistoryById)


module.exports = {voucherHistoryRouter }