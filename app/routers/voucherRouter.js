//Khai báo thư viện express:
const express = require('express');
const { createVoucher, getAllVouchers, getVoucherById, updateVoucherById, deleteVoucherById, ramdomVoucher } = require('../controllers/voucherController');

//Khai báo  router:
const voucherRouter = express.Router();


voucherRouter.post("/vouchers", createVoucher)

voucherRouter.get("/vouchers", getAllVouchers)

voucherRouter.get("/vouchers/:voucherId", getVoucherById)

voucherRouter.put("/vouchers/:voucherId", updateVoucherById)

voucherRouter.delete("/vouchers/:voucherId", deleteVoucherById)



module.exports = { voucherRouter }