//Khai báo thư viện express:
const express = require('express');
const { createPrize, getAllPrizes, getPrizeById, updatePrizeById, deletePrizeById } = require('../controllers/prizeController');

//Khai báo  router:
const prizeRouter = express.Router();


prizeRouter.post("/prizes", createPrize)

prizeRouter.get("/prizes", getAllPrizes)

prizeRouter.get("/prizes/:prizeId", getPrizeById)

prizeRouter.put("/prizes/:prizeId", updatePrizeById)

prizeRouter.delete("/prizes/:prizeId", deletePrizeById)


module.exports = { prizeRouter }