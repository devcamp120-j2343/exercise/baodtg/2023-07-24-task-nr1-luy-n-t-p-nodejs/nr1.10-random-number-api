//Khai báo thư viện express:
const express = require('express');
const { createPrizeHistory, getAllPrizeHistory, getPrizeHistoryById, updatePrizeHistoryById, deletePrizeHistoryById } = require('../controllers/prizeHistoryController');

//Khai báo  router:
const prizeHistoryRouter = express.Router();


prizeHistoryRouter.post("/prize-histories", createPrizeHistory)

prizeHistoryRouter.get("/prize-histories", getAllPrizeHistory)

prizeHistoryRouter.get("/prize-histories/:prizeHistoryId", getPrizeHistoryById)

prizeHistoryRouter.put("/prize-histories/:prizeHistoryId", updatePrizeHistoryById)

prizeHistoryRouter.delete("/prize-histories/:prizeHistoryId", deletePrizeHistoryById)


module.exports = {prizeHistoryRouter }