//Khai báo thư viện express:
const express = require('express');
const { dateTimeMiddleware, methodMiddleware } = require('../middlewares/diceMiddleware');
const { createDice, ramdomVoucher, countVouchers, getDiceHistoryByUserName, getPrizeHistoryByUserName, getVoucherHistoryByUserName } = require('../controllers/diceController');

//Khai báo  router:
const diceRouter = express.Router();

diceRouter.get("/random-number", dateTimeMiddleware, methodMiddleware, (req, res) => {
    let randomNum = Math.floor(Math.random() * 6) + 1;
    console.log(randomNum)
    res.json({
        message: randomNum
    })
} )

diceRouter.post('/devcamp-lucky-dice/dice', createDice)

diceRouter.get('/devcamp-lucky-dice/dice-history', getDiceHistoryByUserName)

diceRouter.get('/devcamp-lucky-dice/prize-history', getPrizeHistoryByUserName)

diceRouter.get('/devcamp-lucky-dice/voucher-history', getVoucherHistoryByUserName)

module.exports = { diceRouter }