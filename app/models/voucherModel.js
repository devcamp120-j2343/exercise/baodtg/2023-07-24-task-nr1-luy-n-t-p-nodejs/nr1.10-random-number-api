//Khai báo thư viện mongoose:
const mongoose = require('mongoose');
//Khai báo thư viện Schema của mongoose:
const Schema = mongoose.Schema;

//B3: tạo đối tượng Schema bao gồm các thuộc tính của collection trong mongoDB:
const voucherSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    code: {
        type: String,
        unique: true,
        required: true
    },
    discount: {
        type: Number,
        required: true
    },
    note: {
        type: String,
        required: false
    },
    createAt: {
        type: Date,
        default: Date.now()
    },
    updateAt: {
        type: Date,
        default: Date.now()
    }
});
//B4: export schema ra model
module.exports = mongoose.model('voucher', voucherSchema)