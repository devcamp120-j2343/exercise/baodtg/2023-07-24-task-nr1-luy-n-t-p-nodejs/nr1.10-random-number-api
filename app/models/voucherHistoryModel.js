//Khai báo thư viện mongoose:
const mongoose = require('mongoose');
//Khai báo thư viện Schema của mongoose:
const Schema = mongoose.Schema;

//B3: tạo đối tượng Schema bao gồm các thuộc tính của collection trong mongoDB:
const voucherHistorySchema = new Schema({
    _id: mongoose.Types.ObjectId,
    user: {
        type: mongoose.Types.ObjectId,
        ref: "user",
        required: true
    },
    voucher: {
        type: mongoose.Types.ObjectId,
        ref: "voucher",
        required: true
    },
    createAt: {
        type: Date,
        default: Date.now()
    },
    updateAt: {
        type: Date,
        default: Date.now()
    }
});
//B4: export schema ra model
module.exports = mongoose.model('voucher-history', voucherHistorySchema)