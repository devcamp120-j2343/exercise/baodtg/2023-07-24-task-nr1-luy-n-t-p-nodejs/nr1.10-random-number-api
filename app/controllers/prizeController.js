//Import model
const { default: mongoose } = require('mongoose');
const prizeModel = require('../models/prizeModel')

const createPrize = async (req, res) => {
    //thu thập dữ liệu:
    const { name, description} = req.body;
    //B2: validate dữ liệu:
    if (!name) {
        return res.status(400).json({
            status: "Bad request",
            message: "name is required !"
        })
    }
    //B3 thực thi model:
    const newPrizeCreateData = {
        _id: new mongoose.Types.ObjectId,
        name,
        description,
    }
    try {
        const prizeCreated = await prizeModel.create(newPrizeCreateData);
        if (prizeCreated) {
            return res.status(201).json({
                status: "Create new prize successfully !",
                data: prizeCreated
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

//get all prize:
const getAllPrizes = async (req, res) => {
    try {
        const prizesList = await prizeModel.find();
        if (prizesList && prizesList.length > 0) {
            return res.status(200).json({
                status: "Get all prizes successfully!",
                data: prizesList
            })
        } else {
            return res.status(404).json({
                status: "Not found any prizes",
                data: prizesList

            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}
//get prize by id:
const getPrizeById = async (req, res) => {
    const prizeId = req.params.prizeId;
    if (!mongoose.Types.ObjectId.isValid(prizeId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `prizecreatedprize Id ${prizeId} is invalid`
        })
    }
    try {
        const prizeFoundById = await prizeModel.findById(prizeId);
        if (prizeFoundById) {
            return res.status(200).json({
                status: `prizecreatedprize found by Id ${prizeId} is successfully !`,
                data: prizeFoundById
            })
        } else {
            return res.status(404).json({
                status: `Not found any prizecreatedprizes by id ${prizeId}`,
                data: prizeFoundById
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

//Update prizecreatedprize by id
const updatePrizeById = async (req, res) => {
    const prizeId = req.params.prizeId;
    const { name, description } = req.body;
    if (!mongoose.Types.ObjectId.isValid(prizeId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `prizecreatedprize Id ${prizeId} is invalid !`
        })
    }
    //validate:
    if (!name) {
        return res.status(400).json({
            status: "Bad request",
            message: "name is required !"
        })
    }
   
    const prizeUpdateData = {
        name,
        description
    }
    try {
        const prizeUpdatedById = await prizeModel.findByIdAndUpdate(prizeId, prizeUpdateData);
        if (prizeUpdatedById) {
            return res.status(200).json({
                status: `Update prizecreatedprize by id ${prizeId} successfully !`,
               data: prizeUpdatedById
            })
        } else{
            return res.status(404).json({
                status: `Not found any prizecreatedprizes by Id ${prizeId}`,
               data: prizeUpdatedById
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}
//delete prizecreatedprize by id
const deletePrizeById = async (req, res) => {
    const prizeId = req.params.prizeId;
    if (!mongoose.Types.ObjectId.isValid(prizeId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `prizecreatedprize Id ${prizeId} is invalid !`
        })
    }
    try {
        const prizeDeletedById = await prizeModel.findByIdAndDelete(prizeId)
        if (prizeDeletedById) {
            return res.status(204).json({
                status: `Delete prizecreatedprize by id ${prizeId} successfully`,
               data: prizeDeletedById
            })
        }else{
            return res.status(404).json({
                status: `Not found any prizecreatedprizes by Id ${prizeId}`,
               data: prizeDeletedById
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}


module.exports = { createPrize, getAllPrizes, getPrizeById, updatePrizeById, deletePrizeById }