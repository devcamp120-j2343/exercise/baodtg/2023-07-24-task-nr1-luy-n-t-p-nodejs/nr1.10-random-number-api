//Import model
const { default: mongoose } = require('mongoose');
const diceHistoryModel = require('../models/diceHistoryModel')

//create dice history:
const createDiceHistory = async (req, res) => {
    const { user } = req.body;
    const dice = Math.floor(Math.random() * 6) + 1;
    const newDiceHistoryData = {
        _id: new mongoose.Types.ObjectId,
        user: user,
        dice
    }
    //validate:
    if (!user) {
        return res.status(400).json({
            status: `Bad request`,
            message: `User id is required !`
        })
    }
    try {
        const diceHistoryCreated = await diceHistoryModel.create(newDiceHistoryData)
        if (diceHistoryCreated) {
            return res.status(201).json({
                status: `Create new dice history successfully !`,
                data: diceHistoryCreated
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

//get all dice histories
const getAllDiceHistories = async (req, res) => {
    let user = req.query.user;
    let condition = {};
    if(user) {
        condition.user = user;    
    }
    console.log(condition)
    try {
        const diceHistoriesList = await diceHistoryModel.find(condition);
        if (diceHistoriesList && diceHistoriesList.length > 0) {
            return res.status(200).json({
                status: `Get all dice histories sucessfully !`,
                data: diceHistoriesList
            })
        } else {
            return res.status(404).json({
                status: `Not found any  dice histories !`,
                data: diceHistoriesList
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }

}

//Get dice history by id
const getDiceHistoryById = async (req, res) => {
    const diceHistoryId = req.params.diceHistoryId;
    if (!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `Dice history id ${diceHistoryId} is invalid`
        })
    }
    try {
        const diceHistoryFoundById = await diceHistoryModel.findById(diceHistoryId);
        if (diceHistoryFoundById) {
            return res.status(200).json({
                status: `Get dice histories by id ${diceHistoryId} sucessfully !`,
                data: diceHistoryFoundById
            })
        } else {
            return res.status(404).json({
                status: `Not found any  dice histories by id ${diceHistoryId} !`,
                data: diceHistoryFoundById
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

//Update dice history by id
const updateDiceHistoryById = async (req, res) => {
    const diceHistoryId = req.params.diceHistoryId;
    const { user, dice } = req.body;
    if (!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `Dice history Id ${diceHistoryId} is invalid !`
        })
    }
    //validate:
    if (!user) {
        return res.status(400).json({
            status: `Bad request`,
            message: `User id is required !`
        })
    }
    if (!dice) {
        return res.status(400).json({
            status: "Bad request",
            message: "dice is required !"
        })
    }

    const diceHistoryUpdateData = {
        user,
        dice,
    }
    try {
        const diceHistoryUpdated = await diceHistoryModel.findByIdAndUpdate(diceHistoryId, diceHistoryUpdateData);
        if (diceHistoryUpdated) {
            return res.status(200).json({
                status: `Update dice history by id ${diceHistoryId} successfully !`,
                data: diceHistoryUpdated
            })
        } else {
            return res.status(404).json({
                status: `Not found any dice history by Id ${diceHistoryId}`,
                data: diceHistoryUpdated
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

//delete user by id
const deleteDiceHistoryById = async (req, res) => {
    const diceHistoryId = req.params.diceHistoryId;
    if (!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `Dice history Id ${diceHistoryId} is invalid !`
        })
    }
    try {
        const diceHistoryDeleted = await diceHistoryModel.findByIdAndDelete(diceHistoryId)
        if (diceHistoryDeleted) {
            return res.status(204).json({
                status: `Delete dice history by id ${diceHistoryId} successfully`,
                data: diceHistoryDeleted
            })
        } else {
            return res.status(404).json({
                status: `Not found any dice histories by Id ${diceHistoryId}`,
                data: diceHistoryDeleted
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}
module.exports = { createDiceHistory, getAllDiceHistories, getDiceHistoryById, updateDiceHistoryById, deleteDiceHistoryById }