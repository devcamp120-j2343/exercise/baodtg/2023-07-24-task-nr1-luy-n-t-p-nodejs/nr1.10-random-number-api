//Import model
const { default: mongoose } = require('mongoose');
const userModel = require('../models/userModel')

const createUser = async (req, res) => {
    //thu thập dữ liệu:
    const { userName, firstName, lastName } = req.body;
    //B2: validate dữ liệu:
    if (!userName) {
        return res.status(400).json({
            status: "Bad request",
            message: "userName is required !"
        })
    }
    if (!firstName) {
        return res.status(400).json({
            status: "Bad request",
            message: "firstName is required !"
        })
    }
    if (!lastName) {
        return res.status(400).json({
            status: "Bad request",
            message: "lastName is required !"
        })
    }
    //B3 thực thi model:
    const newUserData = {
        _id: new mongoose.Types.ObjectId,
        userName,
        firstName,
        lastName
    }
    try {
        const createdUser = await userModel.create(newUserData);
        if (createdUser) {
            return res.status(201).json({
                status: "Create new user successfully !",
                data: createdUser
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

//get all user:
const getAllUsers = async (req, res) => {
    try {
        const userList = await userModel.find();
        if (userList && userList.length > 0) {
            return res.status(200).json({
                status: "Get all users successfully!",
                data: userList
            })
        } else {
            return res.status(404).json({
                status: "Not found any user",
                data: userList

            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}
//get user by id:
const getUserById = async (req, res) => {
    const userId = req.params.userId;
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `User Id ${userId} is invalid`
        })
    }
    try {
        const userFoundById = await userModel.findById(userId);
        if (userFoundById) {
            return res.status(200).json({
                status: `User found by Id ${userId} is successfully !`,
                data: userFoundById
            })
        } else {
            return res.status(404).json({
                status: `Not found any users by id ${userId}`,
                data: userFoundById
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

//Update user by id
const updateUserById = async (req, res) => {
    const userId = req.params.userId;
    const { userName, firstName, lastName } = req.body;
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `User Id ${userId} is invalid !`
        })
    }
    //validate:
    if (!userName) {
        return res.status(400).json({
            status: "Bad request",
            message: "userName is required !"
        })
    }
    if (!firstName) {
        return res.status(400).json({
            status: "Bad request",
            message: "firstName is required !"
        })
    }
    if (!lastName) {
        return res.status(400).json({
            status: "Bad request",
            message: "lastName is required !"
        })
    }
    const UserUpdateData = {
        userName,
        firstName,
        lastName
    }
    try {
        const userUpdated = await userModel.findByIdAndUpdate(userId, UserUpdateData);
        if (userUpdated) {
            return res.status(200).json({
                status: `Update user by id ${userId} successfully !`,
               data: userUpdated
            })
        } else{
            return res.status(404).json({
                status: `Not found any users by Id ${userId}`,
               data: userUpdated
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}
//delete user by id
const deleteUserById = async (req, res) => {
    const userId = req.params.userId;
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `User Id ${userId} is invalid !`
        })
    }
    try {
        const userDeleted = await userModel.findByIdAndDelete(userId)
        if (userDeleted) {
            return res.status(204).json({
                status: `Delete user by id ${userId} successfully`,
               data: userDeleted
            })
        }else{
            return res.status(404).json({
                status: `Not found any users by Id ${userId}`,
               data: userDeleted
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}


module.exports = { createUser, getAllUsers, getUserById, updateUserById, deleteUserById }