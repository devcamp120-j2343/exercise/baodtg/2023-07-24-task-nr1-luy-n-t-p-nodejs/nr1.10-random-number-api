const voucherHistoryModel = require('../models/voucherHistoryModel');
const mongoose = require('mongoose');



const createVoucherHistory = async (req, res) => {
    //thu thập dữ liệu:
    const { user, voucher } = req.body;
    //B2: validate dữ liệu:
    if (!user) {
        return res.status(400).json({
            status: "Bad request",
            message: "user is required !"
        })
    }
    if (!voucher) {
        return res.status(400).json({
            status: "Bad request",
            message: "voucher is required !"
        })
    }
    //B3 thực thi model:
    const newVoucherHistoryCreateData = {
        _id: new mongoose.Types.ObjectId,
        user,
        voucher
    }
    try {
        const voucherHistoryCreated = await voucherHistoryModel.create(newVoucherHistoryCreateData);
        if (voucherHistoryCreated) {
            return res.status(201).json({
                status: "Create new Voucher successfully !",
                data: voucherHistoryCreated
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

//get all Voucher:
const getAllVoucherHistory = async (req, res) => {
    let user = req.query.user;
    let condition = {};
    if(user) {
        condition.user = user;    
    }
    console.log(condition)
    try {
        const voucherHistoryList = await voucherHistoryModel.find(condition);
        if (voucherHistoryList && voucherHistoryList.length > 0) {
            return res.status(200).json({
                status: "Get all Voucher history successfully!",
                data: voucherHistoryList
            })
        } else {
            return res.status(404).json({
                status: "Not found any Voucher history",
                data: voucherHistoryList

            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

//get Voucher by id:
const getVoucherHistoryById = async (req, res) => {
    const voucherHistoryId = req.params.voucherHistoryId;
    if (!mongoose.Types.ObjectId.isValid(voucherHistoryId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `Voucher history Id ${voucherHistoryId} is invalid`
        })
    }
    try {
        const voucherHistoryFoundById = await voucherHistoryModel.findById(voucherHistoryId);
        if (voucherHistoryFoundById) {
            return res.status(200).json({
                status: `Voucher history found by Id ${voucherHistoryId} is successfully !`,
                data: voucherHistoryFoundById
            })
        } else {
            return res.status(404).json({
                status: `Not found any Voucher history by id ${voucherHistoryId}`,
                data: voucherHistoryFoundById
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

//Update Voucher history by id
const updateVoucherHistoryById = async (req, res) => {
    const voucherHistoryId = req.params.voucherHistoryId;
    const { user, voucher } = req.body;
    if (!mongoose.Types.ObjectId.isValid(voucherHistoryId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `Voucher history Id ${voucherHistoryId} is invalid !`
        })
    }
    //validate:
    if (!user) {
        return res.status(400).json({
            status: "Bad request",
            message: "user is required !"
        })
    }
    if (!voucher) {
        return res.status(400).json({
            status: "Bad request",
            message: "voucher is required !"
        })
    }
    const voucherHistoryUpdateData = {
        user,
        voucher
    }
    try {
        const voucherHistoryUpdated = await voucherHistoryModel.findByIdAndUpdate(voucherHistoryId, voucherHistoryUpdateData);
        if (voucherHistoryUpdated) {
            return res.status(200).json({
                status: `Update Voucher history by id ${voucherHistoryId} successfully !`,
                data: voucherHistoryUpdated
            })
        } else {
            return res.status(404).json({
                status: `Not found any Voucher history by Id ${voucherHistoryId}`,
                data: voucherHistoryUpdated
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

//delete Voucher history by id
const deleteVoucherHistoryById = async (req, res) => {
    const voucherHistoryId = req.params.voucherHistoryId;
    if (!mongoose.Types.ObjectId.isValid(voucherHistoryId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `Voucher history Id ${voucherHistoryId} is invalid !`
        })
    }
    try {
        const voucherHistoryDeleted = await voucherHistoryModel.findByIdAndDelete(voucherHistoryId)
        if (voucherHistoryDeleted) {
            return res.status(204).json({
                status: `Delete Voucher history by id ${voucherHistoryId} successfully`,
                data: voucherHistoryDeleted
            })
        } else {
            return res.status(404).json({
                status: `Not found any Voucher history by Id ${voucherHistoryId}`,
                data: voucherHistoryDeleted
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

module.exports = {createVoucherHistory, getAllVoucherHistory, getVoucherHistoryById, updateVoucherHistoryById, deleteVoucherHistoryById}
