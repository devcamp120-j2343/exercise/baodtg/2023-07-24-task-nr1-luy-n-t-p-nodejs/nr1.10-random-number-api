const prizeHistoryModel = require('../models/prizeHistoryModel');
const mongoose = require('mongoose')


//create prize history:
const createPrizeHistory = async (req, res) => {
    //thu thập dữ liệu:
    const { user, prize } = req.body;
    //B2: validate dữ liệu:
    if (!user) {
        return res.status(400).json({
            status: "Bad request",
            message: "user is required !"
        })
    }
    if (!prize) {
        return res.status(400).json({
            status: "Bad request",
            message: "prize is required !"
        })
    }
    //B3 thực thi model:
    const newPrizeHistoryCreateData = {
        _id: new mongoose.Types.ObjectId,
        user,
        prize
    }
    try {
        const prizeHistoryCreated = await prizeHistoryModel.create(newPrizeHistoryCreateData);
        if (prizeHistoryCreated) {
            return res.status(201).json({
                status: `Create new prize history with user ${user} successfully !`,
                data: prizeHistoryCreated
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

//get all prize history:
const getAllPrizeHistory = async (req, res) => {
    let user = req.query.user;
    let condition = {};
    if(user) {
        condition.user = user;    
    }
    console.log(condition)
    try {
        const prizeHistoryList = await prizeHistoryModel.find(condition);
        if (prizeHistoryList && prizeHistoryList.length > 0) {
            return res.status(200).json({
                status: "Get all prize history successfully!",
                data: prizeHistoryList
            })
        } else {
            return res.status(404).json({
                status: "Not found any prize history",
                data: prizeHistoryList

            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

//get prize history by id:
const getPrizeHistoryById  = async (req, res) => {
    const prizeHistoryId = req.params.prizeHistoryId;
    if (!mongoose.Types.ObjectId.isValid(prizeHistoryId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `Prize history Id ${prizeHistoryId} is invalid`
        })
    }
    try {
        const prizeHistoryFoundById = await prizeHistoryModel.findById(prizeHistoryId);
        if (prizeHistoryFoundById) {
            return res.status(200).json({
                status: `Prize history found by Id ${prizeHistoryId} is successfully !`,
                data: prizeHistoryFoundById
            })
        } else {
            return res.status(404).json({
                status: `Not found any Prize history by id ${prizeHistoryId}`,
                data: prizeHistoryFoundById
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

//Update prize history by id
const updatePrizeHistoryById = async (req, res) => {
    const prizeHistoryId = req.params.prizeHistoryId;
    const { user, prize } = req.body;
    if (!mongoose.Types.ObjectId.isValid(prizeHistoryId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `prizecreatedprize Id ${prizeHistoryId} is invalid !`
        })
    }
    //validate:
    if (!user) {
        return res.status(400).json({
            status: "Bad request",
            message: "name is required !"
        })
    }
    if (!prize) {
        return res.status(400).json({
            status: "Bad request",
            message: "name is required !"
        })
    }
    const prizeHistoryUpdateData = {
        user,
        prize
    }
    try {
        const prizeHistoryUpdated = await prizeHistoryModel.findByIdAndUpdate(prizeHistoryId, prizeHistoryUpdateData);
        if (prizeHistoryUpdated) {
            return res.status(200).json({
                status: `Update prize history by id ${prizeHistoryId} successfully !`,
               data: prizeHistoryUpdated
            })
        } else{
            return res.status(404).json({
                status: `Not found any prize history by Id ${prizeHistoryId}`,
               data: prizeHistoryUpdated
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

//delete prizecreatedprize by id
const deletePrizeHistoryById  = async (req, res) => {
    const prizeHistoryId = req.params.prizeHistoryId;
    if (!mongoose.Types.ObjectId.isValid(prizeHistoryId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `prize history Id ${prizeHistoryId} is invalid !`
        })
    }
    try {
        const prizeHistoryDeletedById = await prizeHistoryModel.findByIdAndDelete(prizeHistoryId)
        if (prizeHistoryDeletedById) {
            return res.status(204).json({
                status: `Delete prize history by id ${prizeHistoryId} successfully`,
               data: prizeHistoryDeletedById
            })
        }else{
            return res.status(404).json({
                status: `Not found any prize history by Id ${prizeHistoryId}`,
               data: prizeHistoryDeletedById
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

module.exports = { createPrizeHistory, getAllPrizeHistory, getPrizeHistoryById, updatePrizeHistoryById, deletePrizeHistoryById }