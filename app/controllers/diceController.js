//Import model
const { default: mongoose } = require('mongoose');
const userModel = require('../models/userModel');
const diceHistoryModel = require('../models/diceHistoryModel');
const prizeHistoryModel = require('../models/prizeHistoryModel');
const prizeModel = require('../models/prizeModel');
const voucherHistoryModel = require('../models/voucherHistoryModel');
const voucherModel = require('../models/voucherModel');


const createDice = async (req, res) => {
    try {
        //thu thập dữ liệu:
        const { firstName, lastName, userName } = req.body;
        const randomNum = Math.floor(Math.random() * 6) + 1;
        //validate dữ liệu:
        if (!firstName) {
            return res.status(400).json({
                status: `Bad request`,
                message: `Firstname is required !`
            })
        }
        if (!lastName) {
            return res.status(400).json({
                status: `Bad request`,
                message: `Lastname is required !`
            })
        }
        if (!userName) {
            return res.status(400).json({
                status: `Bad request`,
                message: `Username is required !`
            })
        }
        const userDataObj = {
            _id: new mongoose.Types.ObjectId,
            firstName,
            lastName,
            userName
        }
        const vDiceDataObj = {
            voucher: {},
            dice: 0,
            prize: {}
        }

        //thực thi model:
        const checkUserName = await userModel.findOne({ userName: userName });
        if (checkUserName) {
            console.log(`Username có sẵn, bắt  đầu tạo DiceHistory với user Id ${checkUserName.id}`);
            const newDiceHistoryData = {
                _id: new mongoose.Types.ObjectId,
                user: checkUserName.id,
                dice: randomNum
            }
            const bNewDiceHistoryCreated = await diceHistoryModel.create(newDiceHistoryData)
            console.log(`Kết quả quay số: ${bNewDiceHistoryCreated.dice}`)
            if (bNewDiceHistoryCreated.dice < 3) {
                vDiceDataObj.voucher = null;
                vDiceDataObj.dice = bNewDiceHistoryCreated.dice;
                vDiceDataObj.prize = null;

                return res.status(200).json({
                    diceResult: vDiceDataObj
                })
            }
            else {
                //Nếu dice ra > 3 thì random 1 voucher từ voucherModel

                //   get tổng số lượng voucher từ voucher model
                const voucherCount = await voucherModel.count();
                //lấy random 1 số từ số tổng voucher
                const random = await Math.floor(Math.random() * voucherCount);

                //lấy 1 voucher random từ voucherModel
                const voucherRandom = await voucherModel.findOne().skip(random);

                //truyền dữ liệu voucher random có được vào dữ liệu của DiceDataObj
                vDiceDataObj.voucher = voucherRandom;
                // vDiceDataObj.dice = bNewDiceHistoryCreated.dice;
                // vDiceDataObj.prize = null;

                //Thêm 1 bản ghi mới vào voucher history
                const vVoucherHistoryDataObj = {
                    _id: new mongoose.Types.ObjectId,
                    user: checkUserName.id,
                    voucher: voucherRandom._id
                }
                // const addVoucherHistoryForUser = await voucherHistoryModel.create(vVoucherHistoryDataObj)
                voucherHistoryModel.create(vVoucherHistoryDataObj)

                //lấy 3 lần gieo xúc xắc gần nhất của User:
                const userLast3Dices = await diceHistoryModel.find({ user: checkUserName.id }).sort({ _id: '-1' }).limit(3)
                if (userLast3Dices.length >= 3 && userLast3Dices[0].dice > 3 && userLast3Dices[1].dice > 3 && userLast3Dices[2].dice > 3) {
                    //lấy random 1 prize từ prizeModel

                    //   get tổng số lượng prize từ prize model
                    const prizeCount = await prizeModel.count();

                    //lấy random 1 số từ số tổng prize
                    const random = await Math.floor(Math.random() * prizeCount);

                    //lấy 1 prize random từ prizeModel
                    const prizeRandom = await prizeModel.findOne().skip(random);

                    //truyền dữ liệu prize random có được vào dữ liệu của DiceDataObj
                    vDiceDataObj.prize = prizeRandom;
                    vDiceDataObj.dice = bNewDiceHistoryCreated.dice;

                    //Thêm 1 bản ghi mới vào prize history
                    const vPrizeHistoryDataObj = {
                        _id: new mongoose.Types.ObjectId,
                        user: checkUserName.id,
                        prize: prizeRandom._id
                    }
                    prizeHistoryModel.create(vPrizeHistoryDataObj)

                    return res.status(200).json({
                        diceResult: vDiceDataObj
                    })
                } else {
                    vDiceDataObj.prize = null;
                    vDiceDataObj.dice = bNewDiceHistoryCreated.dice;
                    res.json({
                        diceResult: vDiceDataObj
                    })

                }

            }


        }

        else {
            console.log(`Username không có sẵn, bắt  đầu tạo mới User với userName ${userName}`);
            const newUserCreated = await userModel.create(userDataObj);
            console.log(`Tạo mới user thành công, sau đó tạo mới diceHistory với userId ${newUserCreated._id}`);
            if (newUserCreated) {
                const newDiceHistoryData = {
                    _id: new mongoose.Types.ObjectId,
                    user: newUserCreated.id,
                    dice: randomNum
                }
                const bNewDiceHistoryCreated = await diceHistoryModel.create(newDiceHistoryData)
                if (bNewDiceHistoryCreated) {
                    console.log(`Kết quả quay số: ${bNewDiceHistoryCreated.dice}`)
                    if (bNewDiceHistoryCreated.dice < 3) {
                        vDiceDataObj.voucher = null;
                        vDiceDataObj.dice = bNewDiceHistoryCreated.dice;
                        vDiceDataObj.prize = null;

                        return res.status(200).json({
                            diceResult: vDiceDataObj
                        })
                    } else {
                        //Nếu dice ra > 3 thì random 1 voucher từ voucherModel

                        //   get tổng số lượng voucher từ voucher model
                        const voucherCount = await voucherModel.count();
                        //lấy random 1 số từ số tổng voucher
                        const random = await Math.floor(Math.random() * voucherCount);

                        //lấy 1 voucher random từ voucherModel
                        const voucherRandom = await voucherModel.findOne().skip(random);

                        //Thêm 1 bản ghi mới vào voucher history
                        const vVoucherHistoryDataObj = {
                            _id: new mongoose.Types.ObjectId,
                            user: newUserCreated.id,
                            voucher: voucherRandom._id
                        }
                        voucherHistoryModel.create(vVoucherHistoryDataObj)

                        //truyền dữ liệu voucher random có được vào dữ liệu của DiceDataObj
                        vDiceDataObj.voucher = voucherRandom;
                        vDiceDataObj.dice = bNewDiceHistoryCreated.dice;
                        vDiceDataObj.prize = null;

                        return res.status(200).json({
                            diceResult: vDiceDataObj
                        })



                    }

                }

            }
        }
    } catch (error) {
        return res.status(500).json({
            status: error
        })
    }


}

const getDiceHistoryByUserName = async (req, res) => {
    const userName = req.query.userName;
    const diceHistoryOfUser = []
    const findUserByUserName = await userModel.findOne({ userName: userName });
    console.log(findUserByUserName)

    if (findUserByUserName) {
        const findDiceHistoryOfUser = await diceHistoryModel.find({ user: findUserByUserName.id });
        console.log(findDiceHistoryOfUser)

        for (let i = 0; i < findDiceHistoryOfUser.length; i++) {
            diceHistoryOfUser.push(findDiceHistoryOfUser[i].dice)
        }
        return res.status(200).json({
            dice: diceHistoryOfUser,
            status: 200
        })
    } else {
        return res.status(200).json({
            dice: diceHistoryOfUser,
            status: 200
        })
    }
}


const getPrizeHistoryByUserName = async (req, res) => {
    const userName = req.query.userName;
    const prizeHistoryOfUser = []
    const findUserByUserName = await userModel.findOne({ userName: userName });

    if (findUserByUserName) {
        const findPrizeHistoryOfUser = await prizeHistoryModel.find({ user: findUserByUserName._id });
        console.log(findPrizeHistoryOfUser)
        for (let i = 0; i < findPrizeHistoryOfUser.length; i++) {
            const prize = await prizeModel.find({ _id: findPrizeHistoryOfUser[i].prize })
            for (let e = 0; e < prize.length; e++) {
                prizeHistoryOfUser.push(prize[e].name)


            }
        }
        return res.status(200).json({
            prize: prizeHistoryOfUser,
            status: 200
        })
    } else {
        return res.status(200).json({
            prize: prizeHistoryOfUser,
            status: 200
        })
    }
}

const getVoucherHistoryByUserName = async (req, res) => {
    const userName = req.query.userName;
    const userVoucherHistory = [];
    try {
        const findUserByUserName = await userModel.findOne({ userName });
        if (findUserByUserName) {
            const userId = findUserByUserName.id;
            const findVoucherHistoryOfUser = await voucherHistoryModel.find({ user: userId })
            if (findVoucherHistoryOfUser) {
                userVoucherHistory.push(findVoucherHistoryOfUser) ;
                return res.status(200).json({
                    vouchers: userVoucherHistory
                })
            } else {
                return res.status(200).json({
                    vouchers: userVoucherHistory,
                    message: "User không có voucher"
                })
            }
        } else {
            return res.status(200).json({
                status: "Không tìm thấy user",
                vouchers: userVoucherHistory

            })
        }
    } catch (error) {
        return res.status(500).json({
            status: 'Internal server error',
            message: error.message
        })
    }
}

module.exports = { createDice, getDiceHistoryByUserName, getPrizeHistoryByUserName, getVoucherHistoryByUserName }