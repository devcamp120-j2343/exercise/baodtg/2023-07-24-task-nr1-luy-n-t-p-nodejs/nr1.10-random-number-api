//Import model
const { default: mongoose } = require('mongoose');
const voucherModel = require('../models/voucherModel')

const createVoucher = async (req, res) => {
    //thu thập dữ liệu:
    const { code, discount, note } = req.body;
    //B2: validate dữ liệu:
    if (!code) {
        return res.status(400).json({
            status: "Bad request",
            message: "code is required !"
        })
    }
    if (!discount) {
        return res.status(400).json({
            status: "Bad request",
            message: "discount is required !"
        })
    }
    //B3 thực thi model:
    const newVoucherCreateData = {
        _id: new mongoose.Types.ObjectId,
        code,
        discount,
        note
    }
    try {
        const voucherCreated = await voucherModel.create(newVoucherCreateData);
        if (voucherCreated) {
            return res.status(201).json({
                status: "Create new Voucher successfully !",
                data: voucherCreated
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

//get all Voucher:
const getAllVouchers = async (req, res) => {
    try {
        const vouchersList = await voucherModel.find();
        if (vouchersList && vouchersList.length > 0) {
            return res.status(200).json({
                status: "Get all Vouchers successfully!",
                data: vouchersList
            })
        } else {
            return res.status(404).json({
                status: "Not found any Vouchers",
                data: vouchersList

            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}
//get Voucher by id:
const getVoucherById = async (req, res) => {
    const voucherId = req.params.voucherId;
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `Voucher Id ${voucherId} is invalid`
        })
    }
    try {
        const voucherFoundById = await voucherModel.findById(voucherId);
        if (voucherFoundById) {
            return res.status(200).json({
                status: `Voucher found by Id ${voucherId} is successfully !`,
                data: voucherFoundById
            })
        } else {
            return res.status(404).json({
                status: `Not found any Vouchers by id ${voucherId}`,
                data: voucherFoundById
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

//Update Voucher by id
const updateVoucherById = async (req, res) => {
    const voucherId = req.params.voucherId;
    const { code, discount, note } = req.body;
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `Voucher Id ${voucherId} is invalid !`
        })
    }
    //validate:
    if (!code) {
        return res.status(400).json({
            status: "Bad request",
            message: "code is required !"
        })
    }
    if (!discount) {
        return res.status(400).json({
            status: "Bad request",
            message: "discount is required !"
        })
    }
    const voucherUpdateData = {
        code,
        discount,
        note
    }
    try {
        const voucherUpdatedById = await voucherModel.findByIdAndUpdate(voucherId, voucherUpdateData);
        if (voucherUpdatedById) {
            return res.status(200).json({
                status: `Update Voucher by id ${voucherId} successfully !`,
                data: voucherUpdatedById
            })
        } else {
            return res.status(404).json({
                status: `Not found any Vouchers by Id ${voucherId}`,
                data: voucherUpdatedById
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}
//delete Voucher by id
const deleteVoucherById = async (req, res) => {
    const voucherId = req.params.voucherId;
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `Voucher Id ${voucherId} is invalid !`
        })
    }
    try {
        const voucherDeletedById = await voucherModel.findByIdAndDelete(voucherId)
        if (voucherDeletedById) {
            return res.status(204).json({
                status: `Delete Voucher by id ${voucherId} successfully`,
                data: voucherDeletedById
            })
        } else {
            return res.status(404).json({
                status: `Not found any Vouchers by Id ${voucherId}`,
                data: voucherDeletedById
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}




module.exports = { createVoucher, getAllVouchers, getVoucherById, updateVoucherById, deleteVoucherById }