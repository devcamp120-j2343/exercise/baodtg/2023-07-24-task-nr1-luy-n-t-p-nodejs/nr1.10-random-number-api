const gBASE_URL = "/devcamp-lucky-dice";

//Đối tượng thêm vào:
var diceObj = {
  bigPhoto: "people-playing-casino.jpg",
  greeting: "Chào mừng đến với My Lucky Dice",
  smallPhoto: "dice.png",


}
var gDice = 0;
function onPageLoad() {
  //truy xuat id:
  var divBigPhoto = document.getElementById("big-photo");
  var divGreeting = document.getElementById("greeting");
  // var divSmallPhoto = document.getElementById("small-photo");
  var divMessage = document.getElementById("message");
  //gán đối tượng vào vùng div html:
  divBigPhoto.style.backgroundImage = `url(${diceObj.bigPhoto})`
  divGreeting.innerHTML = diceObj.greeting;
  // divSmallPhoto.style.backgroundImage = `url(${diceObj.smallPhoto})`
  // divMessage.style.display = "none";



}
async function onBtnClickToPlay() {
  console.log("Tung xúc xắc");
  var userName = document.getElementById('inp-username').value;
  var firstName = document.getElementById('inp-firstname').value;
  var lastName = document.getElementById('inp-lastname').value;
  const userInfo = {
    userName,
    firstName,
    lastName
  }
  const newDice = await fetch(gBASE_URL + "/dice",
    {
      method: "POST",
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
      },
      body: JSON.stringify(userInfo)
    }
  )
  const data = await newDice.json()
  const diceResult = data.diceResult.dice
  console.log(diceResult)
  //hàm đổi mặt xúc xắc:
  document.querySelector("#img1").setAttribute("src", diceResult + ".png");
  //hàm đổi thông điệp:
  if (diceResult <= 3) {
    document.querySelector("h2").innerHTML = ("Cảm ơn bạn. Hãy cố gắng lần sau!");          
  } else {
    document.querySelector("h2").innerHTML = ("Chúc mừng bạn. Hãy chơi ván nữa!");
  }
  if (diceResult <= 3){
    document.getElementById("message").style.color = "orange"
  }else{
    document.getElementById("message").style.color = "purple"
  }

}
function getRandomNumberFrom1To6() {
  "use trict";
  var vRandomNumberAppear = Math.floor((Math.random() * 6) + 1);
  return vRandomNumberAppear;
} console.log(getRandomNumberFrom1To6());