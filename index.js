//Khai báo thư viện express:
const express = require('express');

var cors = require('cors')
const { diceRouter } = require('./app/routers/diceRouter');
const path = require("path");

//Import thư viện mongoose
const mongoose = require('mongoose');

//import models
const userModel = require('./app/models/userModel');
const diceHistoryModel = require('./app/models/diceHistoryModel');
const prizeModel = require('./app/models/prizeModel');
const voucherModel = require('./app/models/voucherModel');
const prizeHistoryModel = require('./app/models/prizeHistoryModel');
const voucherHistoryMode = require('./app/models/voucherHistoryModel');

//import Router
const { userRouter } = require('./app/routers/userRouter');
const { diceHistoryRouter } = require('./app/routers/diceHistoryRouter');
const { prizeRouter } = require('./app/routers/prizeRouter');
const { voucherRouter } = require('./app/routers/voucherRouter');
const { prizeHistoryRouter } = require('./app/routers/prizeHistoryRouter');
const { voucherHistoryRouter } = require('./app/routers/voucherHistoryRouter');

//Khai báo app
const app = express();

//Khai báo cổng port:
const port = 8000;
app.use(express.json())
app.use(cors())

// Khai báo APi dạng Get "/" sẽ chạy vào đây
app.get("/", (req, res) => {
    console.log(__dirname);
    res.sendFile(path.join(__dirname + "/views/index.html"));
})
app.use(express.static(__dirname + "/views"))


//Kết nối mongoDB
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_LuckyDiceCasino")
    .then(() => console.log("Connected to Mongo Successfully"))
    .catch(error => handleError(error));

app.use("/", diceRouter);
app.use("/", userRouter);
app.use("/", diceHistoryRouter);
app.use("/", prizeRouter);
app.use("/", voucherRouter);
app.use("/", prizeHistoryRouter);
app.use("/", voucherHistoryRouter);





//Khởi chạy app:
app.listen(port, () => {
    console.log("Chạy project trên cổng 8000")
})